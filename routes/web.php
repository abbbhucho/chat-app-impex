<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ChatController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth'])->name('dashboard');

Route::get('/users/list', [ChatController::class, 'getUsers'])->middleware(['auth'])->name('users_list');

Route::post('conversation/set', [ChatController::class, 'setConversation'])->middleware(['auth'])->name('set_conversation');
Route::get('conversation/get', [ChatController::class, 'getConversation'])->middleware(['auth'])->name('get_conversation');
Route::get('chat-window/{conv_id}', [ChatController::class, 'getChatWindow'])->middleware(['auth'])->name('get_chat_window');
Route::post('save-chat', [ChatController::class, 'save_chat_messages'])->middleware(['auth'])->name('save_chat_messages');

Route::get('test/{conv_id}', function ($conv_id) {
    $ok = \App\Models\Participant::where([
        ['conversation_id', '=', $conv_id],
        ['user_id', '<>', Auth::id()]
    ])->get('user_id');
    dd(array_column($ok->toArray(), 'user_id'));
});

// video

Route::group(['middleware' => ['auth']], function () {
    Route::get('/agora-chat', 'App\Http\Controllers\AgoraVideoController@index');
    Route::post('/agora/token', 'App\Http\Controllers\AgoraVideoController@token');
    Route::post('/agora/call-user', 'App\Http\Controllers\AgoraVideoController@callUser');
});

// Route::post('post-test', function(Illuminate\Http\Request $request){
//     if($request->hasFile('file')){
//         // $fileName = time().'_'.$request->file->getClientOriginalName();
//         // $filePath = $request->file('file')->storeAs('/chat-uploads', $fileName, 'local');
//         return $request->file->getClientOriginalExtension();
//     } else return("no");

// });

Route::post('save-file/{conv_id}', [ChatController::class, 'save_chat_file'])->middleware(['auth']);
Route::get('chats/files/{conv_id}/{auth_id}/{filename}', [ChatController::class, 'getFiles'])->middleware(['auth']);

require __DIR__.'/auth.php';
