<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Ratchet\Server\IoServer;
use Ratchet\Http\HttpServer;
use Ratchet\WebSocket\WsServer;
use React\EventLoop\Factory;
use App\Http\Services\WebSocketService;

class WebSocketServer extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'websocket:init';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'create an instance for websocket using Ratchet';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        try{
            $this->info('server started!');
            $server = IoServer::factory(
                new HttpServer(
                    new WsServer(
                        new WebSocketService()
                    )
                ),
                8090
           );
           $server->run();

        } catch(\Exception $e){
            $this->error('Server Error!' . $e->getMessage());
            // print_r($e->getTraceAsString());
        }
    }
}
