<?php
namespace App\Http\Services;

use App\Models\User;
use Ratchet\ConnectionInterface;
use Ratchet\MessageComponentInterface;
//use Log;

class WebSocketService implements MessageComponentInterface
{
    protected $clients;
    private $subscriptions;
    private $users;
    private $userresources;

    public function __construct()
    {
        $this->clients = new \SplObjectStorage;
        $this->subscriptions = [];
        $this->users = [];
        $this->userresources = [];
        echo "The server is running!\n";
    }

    /**
     * [onOpen description]
     * @method onOpen
     * @param  ConnectionInterface $conn [description]
     * @return [JSON]                    [description]
     * @example connection               var conn = new WebSocket('ws://localhost:8090');
     */
    public function onOpen(ConnectionInterface $conn)
    {
        $this->clients->attach($conn);
        $this->users[$conn->resourceId] = $conn;
    }

    /**
     * [onMessage description]
     * @method onMessage
     * @param  ConnectionInterface $conn [description]
     * @param  [JSON.stringify]              $msg  [description]
     * @return [JSON]                    [description]
     * @example subscribe                conn.send(JSON.stringify({command: "subscribe", channel: "global"}));
     * @example groupchat                conn.send(JSON.stringify({command: "groupchat", message: "hello glob", channel: "global"}));
     * @example message                  conn.send(JSON.stringify({command: "message", to: "1", from: "9", message: "it needs xss protection"}));
     * @example register                 conn.send(JSON.stringify({command: "register", userId: 9}));
     */
    public function onMessage(ConnectionInterface $conn, $msg)
    {
        echo $msg;
        $data = json_decode($msg);
        if (isset($data->command)) {
            switch ($data->command) {
                case "subscribe":
                    $this->subscriptions[$conn->resourceId] = $data->channel;
                break;
                case "groupchat":
                    //#TODO Authentication & authorization using token
                    // $conn->send(json_encode($this->subscriptions));
                    // dump($this->subscriptions, $data, $conn->resourceId);
                    if (isset($this->subscriptions[$conn->resourceId])) {
                        $send_data = [];
                        if(isset($data->time) && !empty($data->time)){
                            $send_data['time'] = $data->time;
                        }
                        if(isset($data->text) && !empty($data->text)){
                            $send_data['text'] = $data->text;
                        }
                        if(isset($data->user_id) && !empty($data->user_id)){
                            $user_details = User::find($data->user_id);
                            $send_data['user'] = $user_details;
                        }
                        $target = $this->subscriptions[$conn->resourceId];
                        foreach ($this->subscriptions as $id=>$channel) {
                            if ($channel == $target && $id != $conn->resourceId) {
                                $this->users[$id]->send(json_encode($send_data));
                            }
                        }
                    }
                break;
                case "message":
                    //
                    if ( isset($this->userresources[$data->to]) ) {
                        foreach ($this->userresources[$data->to] as $key => $resourceId) {
                            if ( isset($this->users[$resourceId]) ) {
                                $send_data = [];
                                if(isset($data->time) && !empty($data->time)){
                                    $send_data['time'] = $data->time;
                                }
                                if(isset($data->text) && !empty($data->text)){
                                    $send_data['text'] = $data->text;
                                }
                                if(isset($data->to) && !empty($data->to)){
                                    $user_details = User::find($data->to);
                                    $send_data['user'] = $user_details;
                                } 
                                $this->users[$resourceId]->send(json_encode($send_data));
                            }
                        }
                        $conn->send(json_encode($this->userresources[$data->to]));
                    }

                    if (isset($this->userresources[$data->from])) {
                        foreach ($this->userresources[$data->from] as $key => $resourceId) {
                            if ( isset($this->users[$resourceId])  && $conn->resourceId != $resourceId ) {
                                $this->users[$resourceId]->send($msg);
                            }
                        }
                    }
                break;
                case "register":
                    //
                    if (isset($data->userId)) {
                        if (isset($this->userresources[$data->userId])) {
                            if (!in_array($conn->resourceId, $this->userresources[$data->userId]))
                            {
                                $this->userresources[$data->userId][] = $conn->resourceId;
                            }
                        }else{
                            $this->userresources[$data->userId] = [];
                            $this->userresources[$data->userId][] = $conn->resourceId;
                        }
                    }
                    $conn->send(json_encode($this->users));
                    $conn->send(json_encode($this->userresources));
                break;
                default:
                    $example = array(
                        'methods' => [
                                    "subscribe" => '{command: "subscribe", channel: "global"}',
                                    "groupchat" => '{command: "groupchat", message: "hello user", channel: "global"}',
                                    "message" => '{command: "message", to: "1", message: "provide a suitable protection"}',
                                    "register" => '{command: "register", userId: 2352}',
                                ],
                    );
                    $conn->send(json_encode($example));
                break;
            }
        }
    }

    public function onClose(ConnectionInterface $conn)
    {
        $this->clients->detach($conn);
        echo "Connection {$conn->resourceId} has disconnected\n";
        unset($this->users[$conn->resourceId]);
        unset($this->subscriptions[$conn->resourceId]);

        foreach ($this->userresources as &$userId) {
            foreach ($userId as $key => $resourceId) {
                if ($resourceId==$conn->resourceId) {
                    unset( $userId[ $key ] );
                }
            }
        }
    }

    public function onError(ConnectionInterface $conn, \Exception $e)
    {
        echo "An error has occurred: {$e->getMessage()}\n";
        $conn->close();
    }
}