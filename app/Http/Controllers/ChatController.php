<?php

namespace App\Http\Controllers;

use Auth;
use Response;
use App\Models\User;
use App\Models\Message;
use App\Models\ChatFile;
use App\Models\Participant;
use App\Models\Conversation;
use Illuminate\Http\Request;
use App\Constants\ChatConstants;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Validator;

class ChatController extends Controller
{
    public function getUsers(){
        try{
            $users = User::whereNotIn('id', [Auth::id()])->get();
            $data = [
                'status' => 'success',
                'message' => 'all users',
                'data' => $users
            ];
            return Response::json($data, 200);
        } catch(\Exception $e) {
            $data = [
                'status' => 'error',
                'message' => $e->getMessage()
            ];
            return Response::json($data, 500);
        }
    }

    public function setConversation(Request $request) {
        try{
            $auth_id = Auth::id();
            $inputs = $request->all();
            // set up conversation
            if (!ctype_digit(implode('',$inputs['selected_users'])) ) {
                return Response::json(['status' => 'error', 'message' => 'invalid data'], 422); 
            }
            
            if($inputs['is_private'] && count($inputs['selected_users']) == ChatConstants::PERSONAL_CHAT){
                $title = User::getUserNameById($inputs['selected_users'][0]);
            } else {
                $title = 'group : ' . implode(", ",array_merge( User::getUserNamesByIds($inputs['selected_users']), [Auth::user()->name] ) );;
            }
            $already_present_conv = Conversation::where('title', $title)->first();
            if(empty($already_present_conv)){
                $converseObj = new Conversation;
                $converseObj->title = $title;
                $converseObj->creator_id = $auth_id;
                $converseObj->is_group = $inputs['is_private'] && count($inputs['selected_users']) == ChatConstants::PERSONAL_CHAT ? false : true;
                $converseObj->deleted_at = null;
                $converseObj->channel_id = null;
                if($converseObj->save()) {
                    // set up participant for the conversation channel
                    $creates[] = [
                        'conversation_id' => $converseObj->id,
                        'user_id' => $auth_id,
                        'type' => ($inputs['is_private'] === 'true') ? ChatConstants::PERSONAL_CHAT : ChatConstants::GROUP_CHAT
                    ];
                    foreach($inputs['selected_users'] as $selected_user){
                        $creates[] = [
                            'conversation_id' => $converseObj->id,
                            'user_id' => $selected_user,
                            'type' => ($inputs['is_private'] === 'true') ? ChatConstants::PERSONAL_CHAT : ChatConstants::GROUP_CHAT
                        ];
                    }foreach($creates as $create){
                        $participantObj = Participant::create($create);
                    }
                    if($participantObj){
                        // $conversation_ids = Participant::where('user_id', $auth_id)->select('conversation_id')->get()->pluck('conversation_id');
                        // $title_arr = [];
                        // foreach($conversation_ids as $conversation_id) {
                        //     $title_arr[] = Conversation::find($conversation_id)->value('title');
                        // }
                        // $data = [
                        //     'data' => [
                        //         'titles' => $title_arr
                        //     ],
                    $data = [ 
                            'status' => 'success', 
                            'message' => 'new conversation group or personal chat created'
                        ];
                        return Response::json($data, 200);
                    }
                }
            } else {
                // $conversation_ids = Participant::where('user_id', $auth_id)->select('conversation_id')->get()->pluck('conversation_id');
                // $title_arr = [];
                // foreach($conversation_ids as $conversation_id) {
                //     $title_arr[] = Conversation::find($conversation_id)->title;
                // }
                $data = [
                    // 'data' => [
                    //     'titles' => $title_arr
                    // ],
                    'status' => 'success', 'message' => 'already present group'
                ];
                return Response::json($data, 200);
            }

        } catch(\Exception $e) {
            $data = [
                'status' => 'error',
                'message' => $e->getMessage()
            ];
             return Response::json($data, 500);
        }
    }

    public function getConversation() {
        try{
            $auth_id = Auth::id();
            $title_arr = [];
            $participantObj = Participant::where('user_id', $auth_id)->get();
            foreach($participantObj as $idx => $participant){
                if($participant->type == ChatConstants::PERSONAL_CHAT){
                    $other_member_obj = Participant::where('conversation_id', $participant->conversation_id)->where('user_id', '!=', $auth_id)->first();
                    $title_arr[$idx]['title'] = $other_member_obj->user->name;
                    $title_arr[$idx]['id'] = $other_member_obj->conversation_id;
                } else if($participant->type == ChatConstants::GROUP_CHAT){
                    $conversation_id = $participant->conversation_id;
                    $title_arr[$idx]['title'] =  Conversation::find($conversation_id)->title;
                    $title_arr[$idx]['id'] = $conversation_id;
                }
            }
            $data = [
                    'data' => $title_arr,
                    'status' => 'success', 'message' => 'all conversations'
                ];
            return Response::json($data, 200);
        } catch(\Exception $e) {
            $data = [
                'status' => 'error',
                'message' => $e->getMessage()
            ];
             return Response::json($data, 500);
        }
    }

    public function getChatWindow($conv_id) {
        try{
            if(!is_int($conv_id) && !is_numeric($conv_id)){
                return Response::json(['status' => 'error', 'message' => 'invalid data' ], 422);
            }
            
            $converseObj = Conversation::find($conv_id);
            if( empty($converseObj) ) {
                return Response::json(['status' => 'error', 'message' => 'conversation deleted or empty' ], 200);
            }
            $participants = Participant::where('conversation_id', $converseObj->id)->get();
            if($converseObj->is_group == false){
                foreach($participants as $participant){
                    if($participant->user_id != Auth::id()){
                        $send_to = $participant->user_id;
                    }
                }
            }

            $messages = Message::where('conversation_id', $converseObj->id)->with('user')->get();
            // #TODO test this $messages object
            if(isset($send_to)){
                $data = [
                    'conversation' => $converseObj,
                    'participants' => $participants,
                    'messages' => $messages,
                    'send_to' => $send_to
                ];    
            } else {
                $data =  [
                    'conversation' => $converseObj,
                    'participants' => $participants,
                    'messages' => $messages
                ];
            }
            return view('components.chat-window', $data);

        } catch(\Exception $e) {
            // $data = [
            //     'status' => 'error',
            //     'message' => $e->getMessage()
            // ];
            //  return Response::json($data, 500);
            return $e->getTraceAsString();
        }
    }

    public function save_chat_messages(Request $request) {
        try{
            if(!$request->has('chat_text') && empty($request->input('chat_text'))) {
                return \Response::json(['message' => 'no chat message'], 422);
            }
            if(!$request->has('conv_id') && empty($request->input('conv_id'))) {
                return \Response::json(['message' => 'incomplete data'], 422);
            }

            if(!$request->has('message_type') || !in_array($request->input('message_type'), array_keys(ChatConstants::MESSAGE_TYPE) ) ) {
                return \Response::json(['message' => 'incomplete data'], 422);
            }
            
            $chat_text = $request->input('chat_text');
            $conv_id = $request->input('conv_id');
            // if it is a file
            switch ($request->input('message_type')) {
                case 'text':
                    $attachment_url = null;
                    $message_type = ChatConstants::MESSAGE_TYPE[$request->input('message_type')];
                    break;
                case 'file':
                    // #TODO create a file save and link address in db
                    break;
                case 'image':
                    // #TODO create a file save and link address in db
                    break;
                case 'audio':
                    // #TODO create a file save and link address in db
                    break;
            }

            $msgObj = new Message();
            $msgObj->conversation_id = $conv_id;
            $msgObj->sender_id = Auth::id();
            $msgObj->message_type = $message_type;
            $msgObj->message = $chat_text;
            $msgObj->attachment_url = $attachment_url;
            $msgObj->deleted_at	= null;
            if($msgObj->save()){
                $messages = Message::where('conversation_id', $conv_id)->get();
                return view('components.chat-block', [
                    'messages' => $messages
                ]);
            }
            
        } catch(\Exception $e) {
            $data = [
                'status' => 'error',
                'message' => $e->getMessage()
            ];
             return Response::json($data, 500);
        }
    }

    public function save_chat_file(Request $request, $conv_id) {
        try{
            $rules = [
                'file' => 'required|mimes:csv,txt,xlsx,pdf,gif,png,jpg,jpeg,doc,docx',
            ];
            $messages = [
                'required' => 'File not provided.',
                'mimes' => 'Invalid File or File format not allowed, read the file formats above',
                // 'max' => 'The file exceeds maximum allowed file size of 4mb.'
            ];

            $validator = Validator::make($request->all(), $rules, $messages);
            if($validator->fails()){
                return Response::json(['status' => 'error', 'message' => 'validation error', 'data' => $validator->errors()], 422);
            }
            if(empty($conv_id) || !is_numeric($conv_id)){
                return Response::json(['status' => 'error', 'message' => 'incorrect conversation'], 422);
            }
            if(empty(Conversation::find($conv_id))){
                return Response::json(['status' => 'error', 'message' => 'incorrect conversation'], 422);
            }
            $auth_id_numeric = (string)(Auth::id());
            if($request->hasFile('file')){

                $chatFileModel = new ChatFile();
                $fileName = time().'_'.$request->file->getClientOriginalName();
                $fileName = str_replace(' ', '_', $fileName);
                $filePath = $request->file('file')->storeAs('/chat-uploads', $fileName, 'local');

                $chatFileModel->name = $fileName;
                $chatFileModel->file_path = '/storage/app/' . $filePath;
                $chatFileModel->file_type = $request->file->getClientOriginalExtension();
                $chatFileModel->conversation_id = $conv_id;
                $chatFileModel->saved_by_user = Auth::id();

                $storage_url = url('/') .'/chats/files/'. (string)$conv_id .'/'. $auth_id_numeric . '/' .$fileName;
                if($chatFileModel->save()){
                    $data = [
                        'status' => 'success',
                        'message' => 'file saved',
                        'data' => [
                            'url' => $storage_url
                        ]
                    ];
                    return Response::json($data, 200);
                }
            } else {
                $data = [
                    'status' => 'error',
                    'message' => 'no file found'
                ];
                return Response::json($data, 422); 
            }
        } catch(\Exception $e) {
            $data = [
                'status' => 'error',
                'message' => $e->getMessage()
            ];
             return Response::json($data, 500);
        }
    }

    public function getFiles($conv_id, $auth_id, $fileName) {
        try{
            $fileModel = ChatFile::where([
                'conversation_id' => $conv_id,
                'saved_by_user' => $auth_id,
                'file_path' => '/storage/app/chat-uploads/' . $fileName
            ])->first();
            if(!empty($fileModel)){
                $pathToFile = base_path() . $fileModel->file_path;
                if(in_Array($fileModel->file_type, ChatConstants::ALLOWED_IMAGE_EXTENSIONS) ){
                    return response()->file($pathToFile);
                }
                return response()->download($pathToFile);
            } else{
                return response()->json(['message' => 'no file found'], 404);
            }
        } catch(\Exception $e) {
            $data = [
                'status' => 'error',
                'message' => $e->getMessage()
            ];
             return Response::json($data, 500);
        }
    }
}
