<?php

namespace App\Http\Controllers;

use Auth;
use App\Models\User;
use App\Models\Participant;
use App\Models\Conversation;
use Illuminate\Http\Request;
use App\Events\MakeAgoraCall;
use App\Class\AgoraDynamicKey\RtcTokenBuilder;

class AgoraVideoController extends Controller
{
    public function index(Request $request)
    {
        if(!( ($request->has('conversation_id') && !empty($request->conversation_id ))
         && ($request->has('conversation_token') && !empty($request->conversation_token)) )){
            return redirect('/dashboard');
        }
        // fetch all users from a given conversation apart from the authenticated user
        $conversation_id = decrypt($request->conversation_id);
        if(empty($conversation_id) || empty(Conversation::find($conversation_id))){
            return redirect('/dashboard')->with(['errormsg' => "Invalid Request, Unable to process data"]);
        }
        $prpant_collec_array = Participant::where([
                        ['conversation_id', '=', $conversation_id],
                        ['user_id', '<>', Auth::id()]
                    ])->get('user_id')->toArray();
        $participants = array_column($prpant_collec_array, 'user_id');
        $users = User::whereIn('id',$participants )->get();
        // $users = User::where('id', '<>', Auth::id())->get();
        return view('agora-chat', ['users' => $users]);
    }

    public function token(Request $request)
    {

        $appID = env('AGORA_APP_ID');
        $appCertificate = env('AGORA_APP_CERTIFICATE');
        $channelName = $request->channelName;
        $user = Auth::user()->name;
        $role = RtcTokenBuilder::RoleAttendee;
        $expireTimeInSeconds = 3600;
        $currentTimestamp = now()->getTimestamp();
        $privilegeExpiredTs = $currentTimestamp + $expireTimeInSeconds;

        $token = RtcTokenBuilder::buildTokenWithUserAccount($appID, $appCertificate, $channelName, $user, $role, $privilegeExpiredTs);

        return $token;
    }

    public function callUser(Request $request)
    {

        $data['userToCall'] = $request->user_to_call;
        $data['channelName'] = $request->channel_name;
        $data['from'] = Auth::id();

        broadcast(new MakeAgoraCall($data))->toOthers();
    }
}
