<?php
namespace App\Constants;

final class ChatConstants
{
    const PERSONAL_CHAT = 1;

    const GROUP_CHAT = 2;

    const MESSAGE_TYPE = [
        'text' => 1,
        'file' => 2,
        'image' => 3,
        'audio' => 4
    ];

    const ALLOWED_IMAGE_EXTENSIONS = ['jpg', 'jpeg', 'png', 'gif'];
    const ALLOWED_FILE_EXTENSIONS = ['doc', 'docx', 'csv', 'xlsx', 'pdf', 'txt'];
}