<script>

    $(document).ready(function() {
        $("#heading-compose").click(function() {
            $(".chat-side-two").css({
                "left": "0%"
            });
        });
        $(".newMessage-back").click(function() {
            $(".chat-side-two").css({
                "left": "-100%"
            });
        });

    /* 
     *   -------- ajax setup --------
     */
    // ajax 
    $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

    // get all conversations
    getConversation();
    });

    //-- Selecting whom to chat with --
    $("#user_lists").on("click", '.new-chat', function(e) {
            {{-- //why only this syntax to know follow this answer https://stackoverflow.com/questions/15090942/event-handler-not-working-on-dynamic-content --}}
                var chk = $(this).find("input[type='checkbox']");
                if ($(this).hasClass("user-selected")) {
                    if (chk.is(":checked") != false) {
                        chk.prop("checked", false);
                    }
                    $(this).removeClass("user-selected");
                } else {
                    if (chk.is(":checked") == false) {
                        chk.prop("checked", true);
                    } else {
                        chk.prop("checked", false);
                    }
                    $(this).addClass("user-selected");
                }
            });

        $("#user-confirm").click(function() {
                var selected_users = [];
                $('#user_lists .user-selected :checkbox:checked').each(function(i) {
                    selected_users[i] = $(this).val();
                });
                console.log(selected_users);
                alert(selected_users);
                    // start new conversation
                var is_private;
                if (isPrivateChat(selected_users)) {
                    is_private = true;
                } else {
                    is_private = false;
                }
                $.ajax({
                    url: "{{ route('set_conversation') }}",
                    method: "POST",
                    data: { is_private: is_private, selected_users: selected_users },
                    }).done(function(response) {
                        console.log(response.data);
                        getConversation();
                    }).fail(function(jqXHR, textStatus) {
                        console.log(textStatus + jqXHR);
                    });
        });

         //-- fetch conversations list :get --
        function getConversation(){
            $.ajax({
                type: 'GET',
                url: '{{route("get_conversation")}}',
                success: function(data) {
                    var obj = data.data;
                    var converse_html = "";
                    
                    if(obj.length){
                        $.each(obj, function(key, val) {
                            converse_html += `<div class="row sideBar-body selected-converse" id="selected_converse_`+ val.id +`">
                                    <div class="col-sm-3 col-xs-3 sideBar-avatar">
                                        <div class="avatar-icon">
                                            <img src="https://bootdey.com/img/Content/avatar/avatar1.png">
                                        </div>
                                    </div>
                                    <div class="col-sm-9 col-xs-9 sideBar-main">
                                        <div class="row">
                                            <div class="col-sm-8 col-xs-8 sideBar-name">
                                                <span class="name-meta">`+val.title+`
                                                </span>
                                            </div>
                                            <div class="col-sm-4 col-xs-4 float-right sideBar-time">
                                                <span class="time-meta float-right">18:18
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>`;
                        });
                    }
                    $("#conversation_lists").html(converse_html);
                },
                error: function(jqXHR, textStatus) {
                    console.log(textStatus + jqXHR);
                }
            });
        }

        //-- fetch users list :get --
        $.ajax({
            type: 'GET',
            url: '{{route("users_list")}}',
            success: function(data) {
                var obj = data.data;
                if (data.status == 'success') {
                    var your_html = "";
                    $.each(obj, function(key, val) {
                        your_html += `<div class="row sideBar-body new-chat">
                                <div class="col-sm-2 col-xs-2 sideBar-checkbox">
                                    <input type="checkbox" value="`+ val.id +`">
                                </div>
                                <div class="col-sm-2 col-xs-2 sideBar-avatar">
                                    <div class="avatar-icon">
                                        <img src="https://bootdey.com/img/Content/avatar/avatar1.png">
                                    </div>
                                </div>
                                <div class="col-sm-8 col-xs-8 sideBar-main">
                                    <div class="row">
                                        <div class="col-sm-8 col-xs-8 sideBar-name">
                                            <span class="name-meta">` + val.name + `</span>
                                        </div>
                                        <div class="col-sm-4 col-xs-4 float-right sideBar-time">
                                            <span class="time-meta float-right">` + val.email + `</span>
                                        </div>
                                    </div>
                                </div>
                            </div>`;
                    });
                    $("#user_lists").html(your_html);
                }
            },
            error: function(data) {
                console.log(data);
            }
        });
        {{-- Selecting conversation block click --}}
        $("#conversation_lists").on("click", '.selected-converse', function(e) {
            // select chat
            $("#conversation_lists .chat-selected").removeClass("chat-selected");
            $(this).addClass("chat-selected");
            // call chat-window and show conversation box
            var id_string = $(this).attr('id');
            var conv_id = id_string.replace('selected_converse_', '');
            
            $.ajax({
                type: "GET",
                url: '{{ route("get_chat_window", "") }}'+'/'+conv_id,
                beforeSend: function() {
                    if( $(".empty-conversation").css('display').toLowerCase() != 'none') {
                        $(".empty-conversation").css("display", "none");
                    }
                    if( $(".conversation").css('display').toLowerCase() != 'none') {
                        $(".conversation").css("display", "none");
                    }
                    if( $(".conversation-loading").css('display').toLowerCase() == 'none') {
                        $(".conversation-loading").css("display", "flex");
                    }
                },
                success: function(response) {
                    $('#conversation_window').html('');
                    $('#conversation_window').html(response);
                    // remove the loader
                    if( $(".conversation-loading").css('display').toLowerCase() != 'none') {
                        $(".conversation-loading").css("display", "none");
                    }
                    // don't show default display
                    if( $(".empty-conversation").css('display').toLowerCase() != 'none') {
                        $(".empty-conversation").css("display", "none");
                    }
                    // show conversation window
                    if( $(".conversation").css('display').toLowerCase() == 'none') {
                        $(".conversation").css("display", "block");
                    }
                    $('#chat-box-scroll-down').scrollTop($('#chat-box-scroll-down')[0].scrollHeight);

                },
                error: function(data) {
                    console.log(data);
                }
            });
        });

    {{-- // helper functions --}}
    function isPrivateChat(arr) {
        arr.sort();
        return arr[0] == arr[arr.length - 1];
    }
</script>