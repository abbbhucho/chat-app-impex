{{-- 
    chat-heading
     --}}
<div class="row chat-heading">
    <div class="col col-lg-1 heading-avatar">
        <div class="heading-avatar-icon">
            <img src="https://bootdey.com/img/Content/avatar/avatar6.png">
        </div>
    </div>
    <div class="col heading-name float-left">
        <span class="heading-name-meta">{{ __($conversation->title) }}
        </span>
        <span class="heading-online">Online</span>
    </div>
    <div class="col col-lg-2 heading-dot-right">
        {{-- <a href="#">
        <!-- call -->
            <svg xmlns="http://www.w3.org/2000/svg" class="icon icon-tabler icon-tabler-phone" width="32" height="32" viewBox="0 0 24 24" stroke-width="1.5" stroke="#93918f" fill="none" stroke-linecap="round" stroke-linejoin="round">
                <path stroke="none" d="M0 0h24v24H0z" fill="none"/>
                <path d="M5 4h4l2 5l-2.5 1.5a11 11 0 0 0 5 5l1.5 -2.5l5 2v4a2 2 0 0 1 -2 2a16 16 0 0 1 -15 -15a2 2 0 0 1 2 -2" />
            </svg>
        </a> --}}
    {{-- #TODO add a conversation token after auth of websockets --}}
        <a target="_blank" href="{{ url('/agora-chat?conversation_id=' . encrypt($conversation->id) . '&conversation_token=12414') }}">
        <!-- video -->
            <svg xmlns="http://www.w3.org/2000/svg" class="icon icon-tabler icon-tabler-video" width="32" height="32" viewBox="0 0 24 24" stroke-width="1.5" stroke="#93918f" fill="none" stroke-linecap="round" stroke-linejoin="round">
                <path stroke="none" d="M0 0h24v24H0z" fill="none"/>
                <path d="M15 10l4.553 -2.276a1 1 0 0 1 1.447 .894v6.764a1 1 0 0 1 -1.447 .894l-4.553 -2.276v-4z" />
                <rect x="3" y="6" width="12" height="12" rx="2" />
            </svg>
        </a>
        <a href="#">
            <i class="fa fa-ellipsis-v fa-2x float-right" aria-hidden="true"></i>
        </a>
    </div>
</div>
{{-- 
    Chat Window
     --}}
<div class="chat-window" id="chat-window">
        <div class="chat-body" id="chat-box-scroll-down">
            @include('components.chat-block', [
                'messages' => $messages,
            ])
        </div>
</div>
{{-- 
    chat-reply
     --}}
{{-- Modal --}}
<div class="modal fade" id="upload-chat-file" tabindex="-1" aria-labelledby="file-upload-chat-modal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content rounded-borders">
            <div class="modal-header">
                <h5 class="modal-title" id="file-upload-chat-modal">
                    File Upload
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="hr-modal">
                <hr style="border-top: 1px solid #b2bec3; width:85%; padding:0 25px 0 25px;">
            </div>
            <div class="modal-body">
                <div class="container-fluid">
                    <div class="alert alert-primary" role="alert">
                        You can either add images(.png, .gid. .jpg, or .jpeg), spreadsheet files(.xlsx, .csv), word document(.doc, .docx), text(.txt) and pdf files <strong>Max File Size allowed</strong> : 4 mb
                    </div>
                    <div class="row">
                        <form class="col-sm-12" type="post" enctype="multipart/form-data" id="chatfile-submit">
                        @csrf
                            <div class="display-file">
                            </div>
                            <input type="file" name="file" class="col-sm-8">
                            <button type="submit" id="chat-window-submit-file" class="btn btn-warning button-center" style="border-radius:8px;"><span class="arc float-right"></span><strong style="margin:2px;"> Save</strong></button>
                        </form>
                    </div>
                    <div id="alert-invalid-file" class="alert alert-danger" role="alert" style="display:none;">
                        <button type="button" class="close" data-dismiss="alert">x</button>
                    </div>
                    <div id="alert-no-file-save" class="alert alert-warning alert-dismissible fade show display_none" role="alert">
                        <strong>Note!</strong> No file uploaded to be send.
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" id="set-fileto-chat" class="btn btn-primary">Save changes</button>
            </div>
        </div>
    </div>
</div>
{{-- Modal End --}}
<form class="row reply" id="chat-form">
    
    <div class="col-sm-1 col-xs-1 reply-emojis">
        <i class="fa fa-smile-o fa-2x"></i>
    </div>
    <div class="col-sm-9 col-xs-8 reply-main">
            <div class="msg-file display_none">
            </div>
            <input type="hidden" name="conv_id" value="{{ $conversation->id }}">
            <input type="text" autocomplete="off" class="form-control" placeholder="Type a message" name="chat_text" id="chat_text" />
    </div>
    <div class="col-sm-1 col-xs-1 reply-recording">
        <button type="button"  data-toggle="modal" data-target="#upload-chat-file" data-converse={{ $conversation->id }}>
        <svg xmlns="http://www.w3.org/2000/svg" class="icon icon-tabler icon-tabler-file-upload" width="28" height="28" viewBox="0 0 24 24" stroke-width="1.5" stroke="#93918f" fill="none" stroke-linecap="round" stroke-linejoin="round">
            <path stroke="none" d="M0 0h24v24H0z" fill="none"/>
            <path d="M14 3v4a1 1 0 0 0 1 1h4" />
            <path d="M17 21h-10a2 2 0 0 1 -2 -2v-14a2 2 0 0 1 2 -2h7l5 5v11a2 2 0 0 1 -2 2z" />
            <line x1="12" y1="11" x2="12" y2="17" />
            <polyline points="9 14 12 11 15 14" />
        </svg>
        </button>
        <button type="button">
        <svg xmlns="http://www.w3.org/2000/svg" class="icon icon-tabler icon-tabler-microphone" width="28" height="28" viewBox="0 0 24 24" stroke-width="1.5" stroke="#93918f" fill="none" stroke-linecap="round" stroke-linejoin="round">
            <path stroke="none" d="M0 0h24v24H0z" fill="none"/>
            <rect x="9" y="2" width="6" height="11" rx="3" />
            <path d="M5 10a7 7 0 0 0 14 0" />
            <line x1="8" y1="21" x2="16" y2="21" />
            <line x1="12" y1="17" x2="12" y2="21" />
        </svg>
        </button>
    </div>
    <div class="col-sm-1 col-xs-1 reply-send">
        <button id="btn-save-chat" type="submit">
            <svg xmlns="http://www.w3.org/2000/svg" class="icon icon-tabler icon-tabler-brand-telegram" width="32" height="32" viewBox="0 0 24 24" stroke-width="1.5" stroke="#93918f" fill="none" stroke-linecap="round" stroke-linejoin="round">
                <path stroke="none" d="M0 0h24v24H0z" fill="none"/>
                <path d="M15 10l-4 4l6 6l4 -16l-18 7l4 2l2 6l3 -4" />
            </svg>
        </button>
    </div>
</form>

<script type="text/javascript">
    @include('scripts.helper-functions');
    $(document).ready(function() {
        const loggedInUserId = {{ auth()->id() }};
        const conversationId = {{ __($conversation->id) }};
        const allowedFileType = ['jpeg', 'jpg', 'png', 'gif', 'pdf', 'doc', 'docx', 'xlsx', 'csv', 'txt'];
        var setFileToMsg = {
            fileName:null
        };

        var conn = new WebSocket('ws://localhost:8090');
        conn.onopen = function(e) {
            console.log("Connection established!");
            @php if ($conversation->is_group == false){ @endphp
            conn.send(JSON.stringify({command: "register", userId: loggedInUserId}));
            @php } else { @endphp
            conn.send(JSON.stringify({command: "subscribe", channel:conversationId }));
            @php } @endphp
        };
        

        conn.onmessage = function(e) {
            var chatDataMsg = JSON.parse(e.data);
            var current_timestamp = Math.round(new Date().getTime()/1000);
            var humanizedTime = moment.duration((chatDataMsg.time - current_timestamp).toString(), 'seconds').humanize(true);
            const $message = $("<div>").addClass("answer").addClass(chatDataMsg.user.id == loggedInUserId ? "right" : "left");
            
            $message.append($("<div>").addClass("name").text(chatDataMsg.user.name));
            $message.append($("<div>").addClass("text").text(chatDataMsg.text));
            $message.append($("<div>").addClass("time").text(humanizedTime));
            
            $('#chat-box-scroll-down').append($message);
            $('#chat-box-scroll-down').scrollTop($('#chat-box-scroll-down')[0].scrollHeight);
        };
    
        {{-- //file upload --}}
        $("#chatfile-submit").submit(function(e) {
            e.preventDefault();
            
            $('#chat-window-submit-file').prop("disabled", true);
            var form = new FormData();
            var files = $('input[type=file]')[0].files[0];
            console.log(files);
            if(!allowedFileType.includes(files.name.split('.').pop())){
                $("#alert-invalid-file").append("<strong>Invalid File Type</strong> You can only add the given file types mentioned above.");
                $("#alert-invalid-file").show();
                $("#alert-invalid-file").delay(8000).slideUp(200, function() {
                    $(this).alert('close');
                    $('#chat-window-submit-file').prop("disabled", false);
                });
                return false;
            }
            form.append('file',files);
            const saveFileUrl = '/save-file/'+ conversationId ;
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: "{{ url('') }}"+saveFileUrl,
                type: "POST",
                data: form,
                contentType: false,
                processData: false,
                beforeSend: function() {
                    if (!$('.arc').hasClass("loader-show")) {
                        $('.arc').addClass('loader-show');
                    }
                },
                success: function(response) {
                    console.log(response);
                    if ($('.arc').hasClass("loader-show")) {
                        $('.arc').removeClass('loader-show');
                    }
                    $('#chat-window-submit-file').prop("disabled", false);
                    var data = getFileHtml(files.name, response.data.url);
                    setFileToMsg.fileName = files.name;
                    $('.display-file').html(data);
                },
                error: function(data, status) {
                    if(data.status == 422){
                        $("#alert-invalid-file").append("<strong>Error!</strong> "+data.responseJSON.data.file);
                    }else{
                        $("#alert-invalid-file").append("<strong>Error!</strong> Something went wrong. Try again later");
                    }
                    $("#alert-invalid-file").show();
                    $("#alert-invalid-file").delay(6000).slideUp(200, function() {
                        $(this).alert('close');
                        $('#chat-window-submit-file').prop("disabled", false);
                        if ($('.arc').hasClass("loader-show")) {
                            $('.arc').removeClass('loader-show');
                            $('#chat-window-submit-file').prop("disabled", false);
                        }
                    });
                    
                }
            });    
        });
        {{-- //include file to chat after saving --}}
        $('#set-fileto-chat').click(function() {
            console.log(setFileToMsg);
            if( setFileToMsg.fileName != null ){
                // has file along
                $('#upload-chat-file').modal('hide');

                $('.msg-file').html(getChatFileDisplayHtml(setFileToMsg.fileName));
                $('.msg-file').removeClass('display_none');
            } else {
                $('#alert-no-file-save').removeClass('display_none');
                $("#alert-no-file-save").delay(3000).slideUp(200, function() {
                        $(this).alert('close');
                    });
            }
            

        });

        {{-- // chat-reply --}}

        $('#chat_text').keyup(function(e) {
            if ($(this).val() == ''){
                if($('#btn-save-chat').hasClass("has-chat")){
                $('#btn-save-chat').removeClass('has-chat');
                }
            }else{
                $('#btn-save-chat').addClass('has-chat');
            }
        });

        {{-- // save chat --}}
        var request;
        $('#chat-form').submit(function(event){
            // Prevent default posting of form - put here to work in case of errors
            event.preventDefault();
            
            // Abort any pending request
            if (request) {
                request.abort();
            }
            var $form = $(this);
            var data = {};

            var $inputs = $form.find("input, select, button, textarea");

            $form.find( '[name]' ).each( function( i , v ){
                var input = $( this ),
                    name = input.attr( 'name' ),
                    value = input.val();
                data[name] = value;
            });
            if(typeof data.chat_text == 'string') {
                data.message_type = 'text';
            } else {
                data.message_type = 'file';
            }
            console.log(data);
            
            $inputs.prop("disabled", true);

            {{--// websocket part --}}
            var currentTimestamp = Math.round(new Date().getTime()/1000);
            
            @php if ($conversation->is_group == false && isset($send_to)){ @endphp
                const sendTo = {{ $send_to }};
                conn.send(JSON.stringify(
                    {
                        command: "message",
                        from: loggedInUserId,
                        to: sendTo,
                        time: currentTimestamp,
                        text: data.chat_text
                        }
                ));
            @php } else { @endphp
                conn.send(JSON.stringify(
                    {
                        command: "groupchat",
                        channel: conversationId,
                        time: currentTimestamp,
                        text: data.chat_text,
                        user_id: loggedInUserId
                    }
                ));
            @php } @endphp
            
            {{-- //sending to DB --}}
            request = $.ajax({
                url: '{{ route("save_chat_messages") }}',
                type: "post",
                data: data
            });

            request.done(function (response, textStatus, jqXHR){
                $('#chat-box-scroll-down').html('');
                $('#chat-box-scroll-down').html(response);
                $("input[name='chat_text']").val('');
                $('#chat-box-scroll-down').scrollTop($('#chat-box-scroll-down')[0].scrollHeight);
            });

            request.fail(function (jqXHR, textStatus, errorThrown){
                console.error(
                    "The following error occurred: "+
                    textStatus, errorThrown
                );
            });

            request.always(function () {
                // Re-enable the inputs
                $inputs.prop("disabled", false);
                if ($('#chat_text').val() == ''){
                    if($('#btn-save-chat').hasClass("has-chat")){
                        $('#btn-save-chat').removeClass("has-chat");
                    }
                }
            });
        });
    });
</script>